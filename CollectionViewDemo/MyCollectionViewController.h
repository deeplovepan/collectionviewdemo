//
//  MyCollectionViewController.h
//  TestCollection
//
//  Created by Peter Pan on 5/11/13.
//  Copyright (c) 2013 Peter Pan. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MyCollectionViewCell;

@interface MyCollectionViewController : UICollectionViewController

@property (strong, nonatomic) NSMutableArray *dataArray;

-(void)customizeForCell:(MyCollectionViewCell*)cell forIndexPath:(NSIndexPath*)indexPath;
-(UICollectionViewFlowLayout*)createLayout;
-(void)cellImageButPressed:(UIButton*)sender;

@end
