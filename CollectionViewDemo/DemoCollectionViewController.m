//
//  DemoCollectionViewController.m
//  CollectionViewDemo
//
//  Created by Peter Pan on 8/17/14.
//  Copyright (c) 2014 Peter Pan. All rights reserved.
//

#import "DemoCollectionViewController.h"
#import "MyCollectionViewCell.h"
#import "CustomCollectionViewFlowLayout.h"  

@interface DemoCollectionViewController ()

@end

@implementation DemoCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    self.collectionView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(UICollectionViewFlowLayout*)createLayout
{
    UICollectionViewFlowLayout *layout = [[CustomCollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(35, 35);
    layout.minimumInteritemSpacing = 25;
    layout.minimumLineSpacing = 15;
    layout.sectionInset = UIEdgeInsetsMake(8, 15, 0, 15) ;
    return layout;
}

-(void)customizeForCell:(MyCollectionViewCell*)cell forIndexPath:(NSIndexPath*)indexPath
{
    NSString *imageName = self.dataArray[indexPath.row];
    [cell.button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}



@end
