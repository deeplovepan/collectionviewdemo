//
//  MyCollectionViewCell.m
//  TestCollection
//
//  Created by Peter Pan on 5/11/13.
//  Copyright (c) 2013 Peter Pan. All rights reserved.
//

#import "MyCollectionViewCell.h"

@implementation MyCollectionViewCell



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.button.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:self.button];
    
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
