//
//  MyCollectionViewController.m
//  TestCollection
//
//  Created by Peter Pan on 5/11/13.
//  Copyright (c) 2013 Peter Pan. All rights reserved.
//

#import "MyCollectionViewController.h"
#import "MyCollectionViewCell.h"


static NSString *cellReuseId = @"CollectionCell";

@interface MyCollectionViewController ()


@end

@implementation MyCollectionViewController

-(UICollectionViewFlowLayout*)createLayout
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(153, 204);
    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = 10;
    return layout;
}

-(id)init
{
    UICollectionViewFlowLayout *layout = [self createLayout];
    
    self = [super initWithCollectionViewLayout:layout];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.collectionView registerClass:[MyCollectionViewCell class]
            forCellWithReuseIdentifier:cellReuseId];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    MyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellReuseId   forIndexPath:indexPath];
    if(cell.button.allTargets.count == 0)
    {
        [cell.button addTarget:self action:@selector(cellImageButPressed:)
              forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    cell.button.tag = indexPath.row;
    [self customizeForCell:cell forIndexPath:indexPath];
    
    return cell;
    
}



#pragma mark - custom method for subclass

-(void)cellImageButPressed:(UIButton*)sender
{
    
}

-(void)customizeForCell:(MyCollectionViewCell*)cell forIndexPath:(NSIndexPath*)indexPath
{
    [cell.button setImage:[UIImage imageNamed:@"dog.jpg"] forState:UIControlStateNormal];
}


@end
