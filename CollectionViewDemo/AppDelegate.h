//
//  AppDelegate.h
//  CollectionViewDemo
//
//  Created by Peter Pan on 8/17/14.
//  Copyright (c) 2014 Peter Pan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
