//
//  CustomCollectionViewFlowLayout.m
//  CollectionViewDemo
//
//  Created by Peter Pan on 8/17/14.
//  Copyright (c) 2014 Peter Pan. All rights reserved.
//

#import "CustomCollectionViewFlowLayout.h"
#import "SeperatorCollectionReusableView.h"

@implementation CustomCollectionViewFlowLayout

- (void)prepareLayout {
    [self registerClass:[SeperatorCollectionReusableView class] forDecorationViewOfKind:@"TopSeperatorCollectionReusableView"];
    [self registerClass:[SeperatorCollectionReusableView class] forDecorationViewOfKind:@"BottomSeperatorCollectionReusableView"];

}

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath {
    // Prepare some variables.
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:indexPath.row+1 inSection:indexPath.section];
    
    UICollectionViewLayoutAttributes *cellAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
    UICollectionViewLayoutAttributes *nextCellAttributes = [self layoutAttributesForItemAtIndexPath:nextIndexPath];
    
    UICollectionViewLayoutAttributes *layoutAttributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:decorationViewKind withIndexPath:indexPath];
    
    CGRect baseFrame = cellAttributes.frame;
    CGRect nextFrame = nextCellAttributes.frame;
    
    CGFloat strokeWidth = 1;
    CGFloat spaceToNextItem = 0;
    if (nextFrame.origin.y == baseFrame.origin.y)
        spaceToNextItem = (nextFrame.origin.x - baseFrame.origin.x - baseFrame.size.width);
    
    float x = baseFrame.origin.x;
    float width = baseFrame.size.width + spaceToNextItem;
    float y = baseFrame.origin.y + baseFrame.size.height + (self.minimumLineSpacing-1)/2;
    
    
    NSInteger lastIndex = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:indexPath.section]-1;


    if(indexPath.row % 5 == 0)
    {
        x = 0;
        width = width + 15;
    }
    else if(indexPath.row == lastIndex ||
            indexPath.row % 5 == 4)
    {
        width = 320 -x;
    }
    
    if([decorationViewKind isEqualToString:@"TopSeperatorCollectionReusableView"])
    {
        y = 0;
    }
    
    
    layoutAttributes.frame = CGRectMake(x,
                                        y,
                                        width,
                                        strokeWidth);

   
    
    layoutAttributes.zIndex = -1;
    return layoutAttributes;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *baseLayoutAttributes = [super layoutAttributesForElementsInRect:rect];
    NSMutableArray * layoutAttributes = [baseLayoutAttributes mutableCopy];
   
    for (UICollectionViewLayoutAttributes *thisLayoutItem in baseLayoutAttributes) {

        if(thisLayoutItem.indexPath.row < 5)
        {
            UICollectionViewLayoutAttributes *item = [self layoutAttributesForDecorationViewOfKind:@"TopSeperatorCollectionReusableView" atIndexPath:thisLayoutItem.indexPath];
            [layoutAttributes addObject:item];
        }
        UICollectionViewLayoutAttributes *item = [self layoutAttributesForDecorationViewOfKind:@"BottomSeperatorCollectionReusableView" atIndexPath:thisLayoutItem.indexPath];
        [layoutAttributes addObject:item];

    }
    
    return layoutAttributes;
}

@end
