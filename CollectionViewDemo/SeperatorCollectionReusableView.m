//
//  SeperatorCollectionReusableView.m
//  CollectionViewDemo
//
//  Created by Peter Pan on 8/17/14.
//  Copyright (c) 2014 Peter Pan. All rights reserved.
//

#import "SeperatorCollectionReusableView.h"

@implementation SeperatorCollectionReusableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor orangeColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
