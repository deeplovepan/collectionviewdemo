//
//  MyCollectionViewCell.h
//  TestCollection
//
//  Created by Peter Pan on 5/11/13.
//  Copyright (c) 2013 Peter Pan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UIButton *button;

@end
